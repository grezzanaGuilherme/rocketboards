﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RocketBoards.Models;

namespace RocketBoards.Controllers
{
    public class FinsController : Controller
    {
        private readonly RocketBoardsContext _context;

        public FinsController(RocketBoardsContext context)
        {
            _context = context;
        }

        // GET: Fins
        public async Task<IActionResult> Index()
        {
            var rocketBoardsContext = _context.Fin.Include(f => f.Brand);
            return View(await rocketBoardsContext.ToListAsync());
        }

        // GET: Fins/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fin = await _context.Fin
                .Include(f => f.Brand)
                .FirstOrDefaultAsync(m => m.FinId == id);
            if (fin == null)
            {
                return NotFound();
            }

            return View(fin);
        }

        // GET: Fins/Create
        public IActionResult Create()
        {
            ViewData["BrandId"] = new SelectList(_context.Brand, "BrandId", "Name");
            return View();
        }

        // POST: Fins/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FinId,BrandId,Material,Model")] Fin fin)
        {
            if (ModelState.IsValid)
            {
                _context.Add(fin);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BrandId"] = new SelectList(_context.Brand, "BrandId", "Name", fin.BrandId);
            return View(fin);
        }

        // GET: Fins/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fin = await _context.Fin.FindAsync(id);
            if (fin == null)
            {
                return NotFound();
            }
            ViewData["BrandId"] = new SelectList(_context.Brand, "BrandId", "Name", fin.BrandId);
            return View(fin);
        }

        // POST: Fins/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FinId,BrandId,Material,Model")] Fin fin)
        {
            if (id != fin.FinId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(fin);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FinExists(fin.FinId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BrandId"] = new SelectList(_context.Brand, "BrandId", "Name", fin.BrandId);
            return View(fin);
        }

        // GET: Fins/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fin = await _context.Fin
                .Include(f => f.Brand)
                .FirstOrDefaultAsync(m => m.FinId == id);
            if (fin == null)
            {
                return NotFound();
            }

            return View(fin);
        }

        // POST: Fins/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var fin = await _context.Fin.FindAsync(id);
            _context.Fin.Remove(fin);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FinExists(int id)
        {
            return _context.Fin.Any(e => e.FinId == id);
        }
    }
}
