﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RocketBoards.Models;

namespace RocketBoards.Controllers
{
    public class ModelBoardsController : Controller
    {
        private readonly RocketBoardsContext _context;

        public ModelBoardsController(RocketBoardsContext context)
        {
            _context = context;
        }

        // GET: ModelBoards
        public async Task<IActionResult> Index()
        {
            var rocketBoardsContext = _context.ModelBoard.Include(m => m.Shaper);
            return View(await rocketBoardsContext.ToListAsync());
        }

        // GET: ModelBoards/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var modelBoard = await _context.ModelBoard
                .Include(m => m.Shaper)
                .FirstOrDefaultAsync(m => m.ModelBoardId == id);
            if (modelBoard == null)
            {
                return NotFound();
            }

            return View(modelBoard);
        }

        // GET: ModelBoards/Create
        public IActionResult Create()
        {
            ViewData["ShaperId"] = new SelectList(_context.Shaper, "ShaperId", "Name");
            return View();
        }

        // POST: ModelBoards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ModelBoardId,Model,ShaperId")] ModelBoard modelBoard)
        {
            if (ModelState.IsValid)
            {
                _context.Add(modelBoard);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ShaperId"] = new SelectList(_context.Shaper, "ShaperId", "Name", modelBoard.ShaperId);
            return View(modelBoard);
        }

        // GET: ModelBoards/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var modelBoard = await _context.ModelBoard.FindAsync(id);
            if (modelBoard == null)
            {
                return NotFound();
            }
            ViewData["ShaperId"] = new SelectList(_context.Shaper, "ShaperId", "Name", modelBoard.ShaperId);
            return View(modelBoard);
        }

        // POST: ModelBoards/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ModelBoardId,Model,ShaperId")] ModelBoard modelBoard)
        {
            if (id != modelBoard.ModelBoardId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(modelBoard);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ModelBoardExists(modelBoard.ModelBoardId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ShaperId"] = new SelectList(_context.Shaper, "ShaperId", "Name", modelBoard.ShaperId);
            return View(modelBoard);
        }

        // GET: ModelBoards/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var modelBoard = await _context.ModelBoard
                .Include(m => m.Shaper)
                .FirstOrDefaultAsync(m => m.ModelBoardId == id);
            if (modelBoard == null)
            {
                return NotFound();
            }

            return View(modelBoard);
        }

        // POST: ModelBoards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var modelBoard = await _context.ModelBoard.FindAsync(id);
            _context.ModelBoard.Remove(modelBoard);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ModelBoardExists(int id)
        {
            return _context.ModelBoard.Any(e => e.ModelBoardId == id);
        }
    }
}
