﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RocketBoards.Models;

namespace RocketBoards.Controllers
{
    public class BuildBoardsController : Controller
    {
        private readonly RocketBoardsContext _context;

        public BuildBoardsController(RocketBoardsContext context)
        {
            _context = context;
        }

        // GET: BuildBoards
        public async Task<IActionResult> Index()
        {
            var rocketBoardsContext = _context.BuildBoard.Include(b => b.Block).Include(b => b.Fin).Include(b => b.ModelBoard);
            return View(await rocketBoardsContext.ToListAsync());
        }

        // GET: BuildBoards/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var buildBoard = await _context.BuildBoard
                .Include(b => b.Block)
                .Include(b => b.Fin)
                .Include(b => b.ModelBoard)
                .FirstOrDefaultAsync(m => m.BuildBoardId == id);
            if (buildBoard == null)
            {
                return NotFound();
            }

            return View(buildBoard);
        }

        // GET: BuildBoards/Create
        public IActionResult Create()
        {
            ViewData["BlockId"] = new SelectList(_context.Block, "BlockId", "Material");
            ViewData["FinId"] = new SelectList(_context.Fin, "FinId", "Material");
            ViewData["ModelBoardId"] = new SelectList(_context.ModelBoard, "ModelBoardId", "Model");
            return View();
        }

        // POST: BuildBoards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("BuildBoardId,BlockId,FinId,ModelBoardId")] BuildBoard buildBoard)
        {
            if (ModelState.IsValid)
            {
                _context.Add(buildBoard);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BlockId"] = new SelectList(_context.Block, "BlockId", "Material", buildBoard.BlockId);
            ViewData["FinId"] = new SelectList(_context.Fin, "FinId", "Material", buildBoard.FinId);
            ViewData["ModelBoardId"] = new SelectList(_context.ModelBoard, "ModelBoardId", "Model", buildBoard.ModelBoardId);
            return View(buildBoard);
        }

        // GET: BuildBoards/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var buildBoard = await _context.BuildBoard.FindAsync(id);
            if (buildBoard == null)
            {
                return NotFound();
            }
            ViewData["BlockId"] = new SelectList(_context.Block, "BlockId", "Material", buildBoard.BlockId);
            ViewData["FinId"] = new SelectList(_context.Fin, "FinId", "Material", buildBoard.FinId);
            ViewData["ModelBoardId"] = new SelectList(_context.ModelBoard, "ModelBoardId", "Model", buildBoard.ModelBoardId);
            return View(buildBoard);
        }

        // POST: BuildBoards/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("BuildBoardId,BlockId,FinId,ModelBoardId")] BuildBoard buildBoard)
        {
            if (id != buildBoard.BuildBoardId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(buildBoard);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BuildBoardExists(buildBoard.BuildBoardId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BlockId"] = new SelectList(_context.Block, "BlockId", "Material", buildBoard.BlockId);
            ViewData["FinId"] = new SelectList(_context.Fin, "FinId", "Material", buildBoard.FinId);
            ViewData["ModelBoardId"] = new SelectList(_context.ModelBoard, "ModelBoardId", "Model", buildBoard.ModelBoardId);
            return View(buildBoard);
        }

        // GET: BuildBoards/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var buildBoard = await _context.BuildBoard
                .Include(b => b.Block)
                .Include(b => b.Fin)
                .Include(b => b.ModelBoard)
                .FirstOrDefaultAsync(m => m.BuildBoardId == id);
            if (buildBoard == null)
            {
                return NotFound();
            }

            return View(buildBoard);
        }

        // POST: BuildBoards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var buildBoard = await _context.BuildBoard.FindAsync(id);
            _context.BuildBoard.Remove(buildBoard);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BuildBoardExists(int id)
        {
            return _context.BuildBoard.Any(e => e.BuildBoardId == id);
        }
    }
}
