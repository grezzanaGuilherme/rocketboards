﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RocketBoards.Models;

namespace RocketBoards.Controllers
{
    public class ShapersController : Controller
    {
        private readonly RocketBoardsContext _context;

        public ShapersController(RocketBoardsContext context)
        {
            _context = context;
        }

        // GET: Shapers
        public async Task<IActionResult> Index()
        {
            var rocketBoardsContext = _context.Shaper.Include(s => s.Brand);
            return View(await rocketBoardsContext.ToListAsync());
        }

        // GET: Shapers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var shaper = await _context.Shaper
                .Include(s => s.Brand)
                .FirstOrDefaultAsync(m => m.ShaperId == id);
            if (shaper == null)
            {
                return NotFound();
            }

            return View(shaper);
        }

        // GET: Shapers/Create
        public IActionResult Create()
        {
            ViewData["BrandId"] = new SelectList(_context.Brand, "BrandId", "Name");
            return View();
        }

        // POST: Shapers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ShaperId,Name,BrandId")] Shaper shaper)
        {
            if (ModelState.IsValid)
            {
                _context.Add(shaper);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BrandId"] = new SelectList(_context.Brand, "BrandId", "Name", shaper.BrandId);
            return View(shaper);
        }

        // GET: Shapers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var shaper = await _context.Shaper.FindAsync(id);
            if (shaper == null)
            {
                return NotFound();
            }
            ViewData["BrandId"] = new SelectList(_context.Brand, "BrandId", "Name", shaper.BrandId);
            return View(shaper);
        }

        // POST: Shapers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ShaperId,Name,BrandId")] Shaper shaper)
        {
            if (id != shaper.ShaperId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(shaper);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ShaperExists(shaper.ShaperId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BrandId"] = new SelectList(_context.Brand, "BrandId", "Name", shaper.BrandId);
            return View(shaper);
        }

        // GET: Shapers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var shaper = await _context.Shaper
                .Include(s => s.Brand)
                .FirstOrDefaultAsync(m => m.ShaperId == id);
            if (shaper == null)
            {
                return NotFound();
            }

            return View(shaper);
        }

        // POST: Shapers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var shaper = await _context.Shaper.FindAsync(id);
            _context.Shaper.Remove(shaper);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ShaperExists(int id)
        {
            return _context.Shaper.Any(e => e.ShaperId == id);
        }
    }
}
