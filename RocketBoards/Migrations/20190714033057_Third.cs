﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RocketBoards.Migrations
{
    public partial class Third : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BuildBoard",
                columns: table => new
                {
                    BuildBoardId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BlockId = table.Column<int>(nullable: false),
                    FinId = table.Column<int>(nullable: false),
                    ModelBoardId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildBoard", x => x.BuildBoardId);
                    table.ForeignKey(
                        name: "FK_BuildBoard_Block_BlockId",
                        column: x => x.BlockId,
                        principalTable: "Block",
                        principalColumn: "BlockId");
                    table.ForeignKey(
                        name: "FK_BuildBoard_Fin_FinId",
                        column: x => x.FinId,
                        principalTable: "Fin",
                        principalColumn: "FinId");
                    table.ForeignKey(
                        name: "FK_BuildBoard_ModelBoard_ModelBoardId",
                        column: x => x.ModelBoardId,
                        principalTable: "ModelBoard",
                        principalColumn: "ModelBoardId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_BuildBoard_BlockId",
                table: "BuildBoard",
                column: "BlockId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildBoard_FinId",
                table: "BuildBoard",
                column: "FinId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildBoard_ModelBoardId",
                table: "BuildBoard",
                column: "ModelBoardId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BuildBoard");
        }
    }
}
