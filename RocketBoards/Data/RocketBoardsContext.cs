﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RocketBoards.Models;

namespace RocketBoards.Models
{
    public class RocketBoardsContext : DbContext
    {
        public RocketBoardsContext (DbContextOptions<RocketBoardsContext> options)
            : base(options)
        {
        }

        public DbSet<RocketBoards.Models.Brand> Brand { get; set; }

        public DbSet<RocketBoards.Models.Shaper> Shaper { get; set; }

        public DbSet<RocketBoards.Models.Fin> Fin { get; set; }

        public DbSet<RocketBoards.Models.ModelBoard> ModelBoard { get; set; }

        public DbSet<RocketBoards.Models.Register> Register { get; set; }

        public DbSet<RocketBoards.Models.Block> Block { get; set; }

        public DbSet<RocketBoards.Models.BuildBoard> BuildBoard { get; set; }
    }
}
