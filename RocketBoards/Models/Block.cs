﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RocketBoards.Models
{
    public class Block
    {

        public int BlockId { get; set; }

        [Display(Name = "Fabricante")]
        [Required(ErrorMessage = "Fabricante é obrigatório")]
        [StringLength(60, MinimumLength = 4)]
        public string Material { get; set; }

        [Display(Name = "Longarina")]
        [Required(ErrorMessage = "Longarina é obrigatório")]
        public bool Stringer { get; set; }

        [Display(Name = "Altura")]
        [Required(ErrorMessage = "Altura é obrigatório")]
        [Range(4, 12, ErrorMessage = "Insert a valid number")]
        public double Height { get; set; }

        [Display(Name = "Largura")]
        [Required(ErrorMessage = "Largura é obrigatório")]
        [Range(5, 30, ErrorMessage = "Insert a valid number")]
        public double Width { get; set; }

        [Display(Name = "Tamanho da Borda")]
        [Required(ErrorMessage = "Tamanho da borda é obrigatório")]
        [Range(2, 20, ErrorMessage = "Insert a valid number")]
        public double Boarder { get; set; }

        [Display(Name = "Shaper")]
        [Required(ErrorMessage = "Shaper é obrigatório")]
        public int ShaperId { get; set; }

        public Shaper Shaper { get; set; }
    }
}
