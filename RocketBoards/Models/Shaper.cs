﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RocketBoards.Models
{
    public class Shaper
    {

        public int ShaperId { get; set; }

        [Display(Name = "Nome do Shaper")]
        [Required(ErrorMessage = "Nome é obrigatório")]
        [StringLength(300, MinimumLength = 4)]
        public string Name { get; set; }

        [Display(Name = "Marca")]
        [Required(ErrorMessage = "Marca é obrigatório")]
        public int BrandId { get; set; }

        public Brand Brand { get; set; }

    }
}
