﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RocketBoards.Models
{
    public class Fin
    {

        public int FinId { get; set; }

        [Display(Name = "Fabricante")]
        [Required(ErrorMessage = "Fabricante é obrigatório")]
        public int BrandId { get; set; }

        public Brand Brand { get; set; }

        [Display(Name = "Material")]
        [Required(ErrorMessage = "Material é obrigatório")]
        [StringLength(60, MinimumLength = 4)]
        public string Material { get; set; }

        [Display(Name = "Modelo")]
        [Required(ErrorMessage = "Modelo é obrigatório")]
        [StringLength(60, MinimumLength = 4)]
        public string Model { get; set; }

    }
}
