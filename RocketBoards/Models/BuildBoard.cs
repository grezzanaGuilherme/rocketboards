﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RocketBoards.Models
{
    public class BuildBoard
    {

        public int BuildBoardId { get; set; }

        [Display(Name = "Bloco")]
        [Required(ErrorMessage = "Longarina é obrigatório")]
        public int BlockId { get; set; }
        public Block Block { get; set; }

        [Display(Name = "Quilhas")]
        [Required(ErrorMessage = "Longarina é obrigatório")]
        public int FinId { get; set; }
        public Fin Fin { get; set; }

        [Display(Name = "Modelo")]
        [Required(ErrorMessage = "Longarina é obrigatório")]
        public int ModelBoardId { get; set; }
        public ModelBoard ModelBoard { get; set; }

    }
}
