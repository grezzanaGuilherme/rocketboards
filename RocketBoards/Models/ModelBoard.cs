﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RocketBoards.Models
{
    public class ModelBoard
    {

        public int ModelBoardId { get; set; }

        [Display(Name = "Modelo")]
        [Required(ErrorMessage = "Modelo é obrigatório")]
        [StringLength(30, MinimumLength = 4)]
        public string Model { get; set; }

        [Display(Name = "Shaper")]
        [Required(ErrorMessage = "Shaper é obrigatório")]
        public int ShaperId { get; set; }

        public Shaper Shaper { get; set; }

    }
}
