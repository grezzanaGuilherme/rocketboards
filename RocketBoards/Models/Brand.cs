﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RocketBoards.Models
{
    public class Brand
    {

        public int BrandId { get; set; }

        [Display(Name = "Marca")]
        [Required(ErrorMessage = "Marca é obrigatório")]
        [StringLength(60, MinimumLength = 4)]
        public string Name { get; set; }

    }
}
